function add_to_path() {
	if ! [[ "$PATH" =~ "$1:" ]]; then
		PATH="$1:$PATH"
	fi
	export PATH
}

function add_to_pkg_config_path() {
	if ! [[ "$PKG_CONFIG_PATH" =~ "$1:" ]]; then
		PKG_CONFIG_PATH="$1:$PKG_CONFIG_PATH"
	fi
	export PKG_CONFIG_PATH
}

function add_to_ld_library_path() {
	if ! [[ "$LD_LIBRARY_PATH" =~ "$1:" ]]; then
		LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH"
	fi
	export LD_LIBRARY_PATH
}

export LOCAL="$HOME/local"

# FORM
add_to_path "$LOCAL/form/bin"

# FriCAS
add_to_path "$LOCAL/fricas/bin"

# Eigen
add_to_pkg_config_path "$LOCAL/eigen/share/pkgconfig"

# cdist
add_to_path "$HOME/git/cdist/bin"

# lhapdf
add_to_path "$LOCAL/lhapdf/bin"
add_to_ld_library_path "$LOCAL/lhapdf/lib"
export LHAPDF_DATA_PATH=$LOCAL/lhapdf/share/LHAPDF/

# Futhark
add_to_path "$LOCAL/futhark/bin"

# QD
add_to_ld_library_path "$LOCAL/qd/lib"
add_to_path "$LOCAL/qd/bin"
add_to_pkg_config_path "$LOCAL/qd/lib/pkgconfig"

# NJet
add_to_ld_library_path "$LOCAL/njet/lib"
add_to_pkg_config_path "$LOCAL/njet/lib/pkgconfig"
